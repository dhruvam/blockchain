import { FilcoinPage } from './app.po';

describe('filcoin App', () => {
  let page: FilcoinPage;

  beforeEach(() => {
    page = new FilcoinPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
