import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DowntimePageComponent } from './downtime-page.component';

describe('DowntimePageComponent', () => {
  let component: DowntimePageComponent;
  let fixture: ComponentFixture<DowntimePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DowntimePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DowntimePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
