import { Component, OnInit } from '@angular/core';
import { LinkService } from '../service/link.service';
@Component({
  selector: 'app-activity-page',
  templateUrl: './activity-page.component.html',
  styleUrls: ['./activity-page.component.css']
})
export class ActivityPageComponent implements OnInit {
 
  
  limit=10;
  data=[];
  in_transactions=[];
  pages=0;
  page_remainder=0;
  check=false;
  offset=0;
  constructor(private link:LinkService) {
    
  }
  ngOnInit() {
    
    console.log(this.link.getTransactionData().local.in_transactions);
    this.changeTransactions();
    
  }
  
  
  range = (value) => { 
    let a = []; 
    for(let i = 0; i < value; ++i) { 
      a.push(i+1); 
    } 
    return a; 
  }

  generateArray=(offset,limit)=>{
    let a=[];
    for(let i = offset; i < limit; ++i) { 
      a.push(Object.keys(this.data)); 
    }
    return a;
  }

  seeTransaction(offset,limit){
    this.offset=offset;
    this.limit=limit;
    this.link.changeTransactionsCount(offset.toString,limit.toString);
    this.changeTransactions();
  }

  changeTransactions()
  {
    if((Object.keys(this.link.getTransactionData().local.in_transactions).length)%(parseInt(this.link.limit))>0){
      this.page_remainder=1;
    }
    this.pages=(this.page_remainder)+((Object.keys(this.link.getTransactionData().local.in_transactions).length)/(parseInt(this.link.limit))^0);
    console.log(this.pages);
    this.limit=parseInt(this.link.limit);
    
    this.data=this.link.getTransactionData().local.in_transactions;
    if(this.pages==1)
    this.check=false;
  }

  setPage(pageNo){
    this.offset=((pageNo-1)*this.limit);
    this.seeTransaction(this.offset,this.limit);
  }

}