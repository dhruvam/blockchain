
import { LandingPageComponent } from './landing-page/landing-page.component';
import { RewardPageComponent } from './reward-page/reward-page.component';
import { SettingsComponent } from './settings/settings.component';
import { TokenComponent } from './token/token.component';
import { ActivityPageComponent } from './activity-page/activity-page.component';
import { DowntimePageComponent } from './downtime-page/downtime-page.component';
import { Routes, RouterModule } from "@angular/router";
import { AppComponent } from './app.component'


const APP_ROUTES:Routes=[
   
    {
        path:'', component: LandingPageComponent
    },
      {
        path:'rewards/:username', component: RewardPageComponent
    },
    {
        path:'account-activity/:username', component: ActivityPageComponent
    },
     {
        path:'downtimeError', component: DowntimePageComponent
    },
    {
        path:'settings/:username', component: SettingsComponent
    },
    {
        path:'account/:username',  component:TokenComponent
    },
    
    

  


];

export const APP_ROUTES_PROVIDER = RouterModule.forRoot(APP_ROUTES);