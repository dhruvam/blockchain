import { Component } from '@angular/core';
import {NavbarService}  from './navbar.service';
import { PostService } from '../service/post.service';
import { LinkService } from '../service/link.service';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],

})
export class NavbarComponent  {
  user;
  checkForPage:boolean=false;
  constructor(private navservice:NavbarService,private post:PostService,private link:LinkService,private route: ActivatedRoute,private ro:Router) { 
   this.checkForPage=navservice.getlocation();
   
  }
  signOut(){

this.checkForPage=false;
  this.ro.navigate(['/']);

  }
  link1(){
    this.user = this.route.snapshot.params['username'];
   
   this.link.getpage(this.user);
  }
  link2(){
     this.user = this.route.snapshot.params['username'];
     
this.link.getactivity(this.user);
  }
  link3(user){
     this.user = this.route.snapshot.params['username'];
  
    this.link.change(this.user);
  }
  link4(user){
     this.user = this.route.snapshot.params['username'];

    this.link.initial(this.user);
  }
}
