import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import { PostService } from '../service/post.service';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  //@Input() page=this.page+1;
   loginForm = new FormGroup({
    username:new FormControl('',Validators.required),
    password1:new FormControl('',Validators.required),
    password2:new FormControl('',Validators.required)
});
  constructor(private post:PostService) {
    
   }

  ngOnInit() {
  }

  changePassword(username:String,password1:String,password2:String){
    if(password1!=password2){
      alert('password does not match');
    }
    else{
      this.post.changePassword(username,password1);
    }
  }

}
