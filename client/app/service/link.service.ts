import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions,URLSearchParams } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { Router } from '@angular/router';
import {ActivityPageComponent} from '../activity-page/activity-page.component';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Injectable()
export class LinkService {
log;
response;
offset="0";
limit="1";
username;

  constructor(private http:Http,private router:Router) { }
getpage(username){
   let headers = new Headers();
     headers.append('Content-Type', 'application/json');
    
    this.http.get('http://localhost:3000/rewards/'+username,  {
    headers: headers
  }).subscribe(
      (res) => {this.log=res;
        this.response=this.log._body;
        console.log(this.response);
        if(this.response==='failure'){
          console.log('faliyre');
        }
        else if(this.response=='success')
        {     console.log('sucess');
              this.router.navigate(['/rewards',username]);

        }
      }
    );
}
getactivity(username){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let params: URLSearchParams = new URLSearchParams();
    params.set('offset', this.offset);
    params.set('limit', this.limit);
    console.log(params);
    let requestOptions=new RequestOptions;
    this.username=username;
    requestOptions.search = params;
    this.http.get('http://localhost:3000/account-activity/'+username,  {
    headers: headers,search:params
    
  }).map((res)=>res.json())
  .subscribe(
      (res) => {
        this.response=res;
        console.log(res);
        if(this.response==='failure'){
          console.log('faliure');
        }
        else if(true)
        {     console.log('success');
              this.router.navigate(['/account-activity',username]);
        }
      },
      (err)=>console.log('error')
    );
}
change(username){
  let headers = new Headers();
     headers.append('Content-Type', 'application/json');
     
    this.http.get('http://localhost:3000/settings/'+username,  {
    headers: headers
  }).subscribe(
      (res) => {this.log=res;
        this.response=this.log._body;
        console.log(res);
        if(this.response==='failure'){
          console.log('faliyre');
        }
        else if(this.response=='success')
        {     console.log('sucess');
              this.router.navigate(['/settings',username]);

        }
        else{
          console.log('fail');
              
        }
      }
    );
}
initial(username){
  let headers = new Headers();
     headers.append('Content-Type', 'application/json');
     
    this.http.get('http://localhost:3000/account/'+username,  {
    headers: headers
  }).subscribe(
      (res) => {this.log=res;
        console.log(res);
        this.response=this.log._body;
        console.log(this.response);
        if(this.response==='failure'){
          console.log('faliyre');
        }
        else if(this.response=='success')
        {     console.log('sucess');
              this.router.navigate(['/account',username]);

        }
      }
    );
}

changeTransactionsCount(offset:string,limit:string){
  this.offset=offset;
  this.limit=limit;
  this.getactivity(this.username);
}

getTransactionData():any{
  return this.response;
}


}
