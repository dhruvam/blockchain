import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Injectable()

export class PostService {

 //Resolve HTTP using the constructor
  constructor (private http: Http,private router:Router) {}
// private instance variable to hold base url
  
  //private postUrl = 'https://jsonplaceholder.typicode.com';
  // Fetch all existing comments
    logError;
    response:string;
    responseBool:boolean;
    result;
   

   authenticate(username, password):any {
    
    //console.log(username);
    let creds = JSON.stringify({ username: username, password: password });
    
    //console.log(creds);
    let headers = new Headers();
     headers.append('Content-Type', 'application/json');
    
    //JSON.stringify()
    this.http.post('http://localhost:3000/login', creds, {
    headers: headers
  }).subscribe(
      (res) => {this.logError=res;
                console.log(res);

        this.response=this.logError._body;
        console.log(this.response);
        
        if(this.response == 'failure'){
           res.ok=false;
          console.log(res);
          console.log('faliure--55');
        }
        if(this.response == 'success')
        {     
             
              console.log("success--55");
              this.router.navigate(['/account',username]);

        }
      }
      
    );
  }
/*get():boolean{
  console.log(this.iserror);
  return this.iserror;
}*/
  signUp(username:string,password1:string):any{
    let creds = JSON.stringify({ username: username, password: password1 });
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.post('http://localhost:3000/signup', creds, {
    headers: headers
    })
    .subscribe(
      (res) => {this.logError=res;
        console.log(res);
        this.response=this.logError._body;
                console.log(this.response);
        if(this.response == 'failure'){
          
          console.log('faliure--55');
        }
        if(this.response == 'success')
        {     
              
              console.log("success--55");
              this.router.navigate(['/account',username]);

        }
      }
    );
 
  }


 

changePassword(username:String,password:String){
    let creds = JSON.stringify({ password: password });
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.put('http://localhost:3000/settings/change-password/'+username, creds, {
    headers: headers
    })
    .subscribe(
      (res) => {this.logError=res;
        console.log(res);
        this.response=this.logError._body;
        console.log(this.response);
        if(this.response == 'error'){
          
          console.log('error');
        }
        if(this.response == 'success')
        {     
              console.log("password changed");
        }
      }
    );
  }

}
