import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { APP_ROUTES_PROVIDER } from './app.routes';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LoginFormComponent } from './landing-page/login-form/login-form.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RewardPageComponent } from './reward-page/reward-page.component';
import { ActivityPageComponent } from './activity-page/activity-page.component';
import { DowntimePageComponent } from './downtime-page/downtime-page.component';
import { SettingsComponent } from './settings/settings.component';
import { QrComponent } from './token/qr/qr.component';
import { QRCodeModule } from 'angular2-qrcode';
import {NavbarService}  from './navbar/navbar.service';
import { TokenComponent } from './token/token.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TeamCardsComponent } from './landing-page/team-cards/team-cards.component';
import { ProvideServicesComponent } from './landing-page/provide-services/provide-services.component';
import { PostService } from './service/post.service';
import { LinkService } from './service/link.service';
import { HttpModule, RequestOptions } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    LoginFormComponent,
    NavbarComponent,
    RewardPageComponent,
    ActivityPageComponent,
    DowntimePageComponent,
    SettingsComponent,
    QrComponent,
    TokenComponent,
    TeamCardsComponent,
    ProvideServicesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    QRCodeModule,
    APP_ROUTES_PROVIDER,
    ReactiveFormsModule
    
  ],
  providers: [NavbarService,PostService,LinkService,ActivityPageComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
