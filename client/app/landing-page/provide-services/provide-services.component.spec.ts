import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvideServicesComponent } from './provide-services.component';

describe('ProvideServicesComponent', () => {
  let component: ProvideServicesComponent;
  let fixture: ComponentFixture<ProvideServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvideServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvideServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
