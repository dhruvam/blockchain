import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PostService } from '../../service/post.service';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],

})
export class LoginFormComponent implements OnInit {
  response;
  
  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
});

  signUpForm = new FormGroup({
    username:new FormControl('',Validators.required),
    email:new FormControl('',Validators.required),
    password1:new FormControl('',Validators.required),
    password2:new FormControl('',Validators.required)
  })

isValid = true;
iserror = false;
signupCard(){
  if(this.isValid===true) this.isValid=false;
  else this.isValid=true;
}



  constructor(private router:Router,private post:PostService) { }


  signIn(username:string,password:string){
  //this.router.navigate(['/account']);
  /*shantanu's edit*/
 
   this.post.authenticate(username,password);
   //this.iserror=this.post.get();
 
  }
 
  

  ngOnInit() {
  }
signUp(username:string,password1:string,password2:string){
  if(password1!=password2){
alert('enter your password again');
  }
     alert("authentication started");
    this.post.signUp(username,password1);
   
        
}
}
