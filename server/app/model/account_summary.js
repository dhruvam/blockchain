var mongoose = require('mongoose');
var account_summarySchema = mongoose.Schema({

    local            : {
        in_transactions     : {
                                in_1:String,
                                in_2:String,
                                in_3:String,

                            },
        out_transactions    : {
                                out_1:String,
                                out_2:String,
                                out_3:String,
                            },
        balance             : String,
        user_id             :String,
          
    },
    
},{ strict: false });


// create the model for account_summary and expose it to our app
module.exports = mongoose.model('Account_Summary', account_summarySchema);
