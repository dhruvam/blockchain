var path=require('path');
var User = require('../model/user');
module.exports=function(app){
    app.put('/settings/change-password/:id',function(req,res){
        User.findOne({'local.username':req.params.id},function(err,user){
             // if there are any errors, return the error
            if (err){
                    console.log('found error');
                    res.send('error');
                    //return done(err);
            }
            if (user) {
                console.log('user found'); 
                    //encrypting the password
                    user.local.password=user.generateHash(req.body.password);
                    user.save(function(err) {
                    if (err)
                        res.send('error')
                        else{
                            console.log('saved!');
                            res.send('success');
                        }
                    //return done(null, newAccount);
                });
                    
            } 
            else {
                    res.send('no id');
            }
        });
    });
}