var path=require('path');
// load up the account-summary model
var AccountSummary = require('../model/account_summary');
module.exports=function(app){
    app.get('/account-activity/:username', function(req, res) {
        var offset=parseInt(req.query.offset);
        var limit=parseInt(req.query.limit);
        AccountSummary.find({'local.user_id' :  req.params.username},{limit:limit,select:'local.in_transactions',skip:offset}).exec(function(err,account_data){
         if(err) res.send('error');
         if(!account_data){
             var newAccount            = new AccountSummary();
                // set the user's local credentials
                newAccount.local.user_id    = req.params.username;
                newAccount.local.in_transactions.in_1 = "12356";
                newAccount.local.in_transactions.in_2 = "12356";
                newAccount.local.in_transactions.in_3 = "12356";
                newAccount.local.out_transactions.out_1 = "12356";
                newAccount.local.out_transactions.out_2 = "12356";
                newAccount.local.out_transactions.out_3 = "12356";
                newAccount.local.balance="4568"
                console.log('user saved'+ newAccount);
                newAccount.save(function(err) {
                    if (err)
                        throw err;
                    newAccount.find({'local.user_id' :  req.params.username}).select('local.in_transactions').skip(offset).limit(limit).exec(function(err,account_data){
                    if (err)
                        throw err;
                         else{
                             console.log(account_data)
                             res.send(account_data);
            }
                    });
            })
         }
         else{
                console.log(account_data);
                //var data=account_data.slice(offset,offset+limit);
                
                res.send(account_data);
            }
        });
         
    });
    
    app.put('/account-activity/update_in_transactions/:id', function(req, res) {
         AccountSummary.findOne({ 'local.user_id' :  req.params.id }, function(err, account) {
            // if there are any errors, return the error
            if (err){
                    console.log('found error');
                    //return done(err);
            }
                if (account) {
                console.log(account);
                account.local.in_transactions.in_1 = "12356";
                account.local.in_transactions.in_2 = "1235ksjbdj6";
                account.local.in_transactions.in_3 = "12356";

                account.save(function(err) {
                    if (err)
                        throw err;
                        else{
                            console.log('saved!');
                        }
                    //return done(null, newAccount);
                });
                //return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            } else {

               
            }

        });    
        res.send('successful');
    });    

    app.put('/account-activity/update_out_transactions/:id', function(req, res) {
         AccountSummary.findOne({ 'local.user_id' :  req.params.id }, function(err, account) {
            // if there are any errors, return the error
            if (err){
                    console.log('found error');
                    //return done(err);
            }
                if (account) {
                console.log(account);
                account.local.out_transactions.out_1 = "12356";
                account.local.out_transactions.out_2 = "12356";
                account.local.out_transactions.out_3 = "12356";
                 account.save(function(err) {
                    if (err)
                        throw err;
                        else{
                            console.log('saved!');
                        }
                    //return done(null, newAccount);
                });
                //return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            } else {

             
            }

        });    
        res.send('successful');
    });    

    app.put('/account-activity/update_balance/:id', function(req, res) {
         AccountSummary.findOne({ 'local.user_id' :  req.params.id }, function(err, account) {
            // if there are any errors, return the error
            if (err){
                    console.log('found error');
                    //return done(err);
            }
                if (account) {
                console.log(account);
                account.local.balance = "12356";
                 account.save(function(err) {
                    if (err)
                        throw err;
                        else{
                            console.log('saved!');
                        }
                    //return done(null, newAccount);
                });
                //return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            } else {

             
            }

        });    
        res.send('successful');
    });    

    app.delete('/account-activity/delete/:id', function(req, res) {
         AccountSummary.findOneAndRemove({ 'local.user_id' :  req.params.id }, function(err) {
            // if there are any errors, return the error
            if (err){
                    console.log('found error');
                    res.send(`id: ${req.params.id} not present!`);
            }
                 else {

                    res.send('successful');
             }

        });    
      
    });    
    
    
  

}