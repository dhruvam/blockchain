var LocalStrategy   = require('passport-local').Strategy;

// load up the user model
var User  = require('../model/user');
var path=require('path');

module.exports = function(app, passport) {


    app.get('/', function(req, res) {
       
    });

    
   
    

    app.get('/rewards/:username',function(req,res){

        res.send('success');
    })

    app.get('/settings/:username',function(req,res){
        res.send('success');
    })



   


   
    app.get('/logout', function(req, res) {
        res.redirect('/');
    })

    //Post SignUp
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/successSignup', 
        failureRedirect : '/failureSignup', 
        failureFlash : true 
    }));
    app.get('/failureSignup', function(req, res) {
    res.send('failure');
    });
    app.get('/successSignup', function(req, res) {
    res.send('success');
    });

    // Post Login
    app.post('/login', passport.authenticate('local-login', {
        
        successRedirect : '/successLogin', 
        failureRedirect: '/failureLogin', 
        failureFlash : true 
        }));

    app.get('/failureLogin', function(req, res) {
        console.log('fail');
    res.send('failure');
    });

    app.get('/successLogin', function(req, res) {
        console.log('yes');
    res.send('success');
});

    /*app.get('/changePassword',function(req,res){
        console.log('change');
       
    })*/

    app.put('/changePassword',function(req,res){
        console.log('change');
        res.send('changePassword');
    })


}

function isLoggedIn(req, res, next) {

    
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

function checkID(req,res,next){
    console.log(User.id+".........");
}