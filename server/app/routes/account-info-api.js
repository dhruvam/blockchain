var path=require('path');
// load up the account-summary model
var AccountInfo = require('../model/account_info');
module.exports=function(app){
    app.get('/account/:username', function(req, res) {
         AccountInfo.findOne({ 'local.user_id' :  req.params.username }, function(err, account) {
            // if there are any errors, return the error
            if (err){
                    console.log('found error');
                    return done(err);
            }
                if (account) {
                console.log(account);
                //return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            } else {

                // if there is no user with that email
                // create the user
                var newAcc            = new AccountInfo();
                // set the user's local credentials
                newAcc.local.user_id    = req.params.username;
                newAcc.local.acc_id.id_1 = "1373783348";
                newAcc.local.acc_id.id_2 = "1349340393";
                newAcc.local.token.tok_1 = "ferrari";
                newAcc.local.token.tok_2 = "audi";
                newAcc.local.token.tok_3 = "mercedes";
                console.log('user saved'+ newAcc);
                newAcc.save(function(err) {
                    if (err)
                        throw err;
                    //return done(null, newAccount);
                });
            }

        });    
        res.send('success');
    });

    app.put('/account/update_acc-id/:username', function(req, res) {
         AccountInfo.findOne({ 'local.user_id' :  req.params.username }, function(err, account) {
            // if there are any errors, return the error
            if (err){
                    console.log('found error');
                    //return done(err);
            }
                if (account) {
                console.log(account);
                account.local.acc_id.id_1 = "123565859459844";
                account.local.acc_id.id_2 = "1235ksjbdj65049";
                

                account.save(function(err) {
                    if (err)
                        throw err;
                        else{
                            console.log('saved!');
                        }
                    //return done(null, newAccount);
                });
                //return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            } else {

               
            }

        });    
        res.send('success');
    });    

    app.put('/account/update_tokens/:username', function(req, res) {
         AccountInfo.findOne({ 'local.user_id' :  req.params.username }, function(err, account) {
            // if there are any errors, return the error
            if (err){
                    console.log('found error');
                    //return done(err);
            }
                if (account) {
                console.log(account);
                account.local.token.tok_1 = "maruti";
                account.local.token.tok_2 = "honda";
                account.local.token.tok_3 = "tata";
                 account.save(function(err) {
                    if (err)
                        throw err;
                        else{
                            console.log('saved!');
                        }
                    //return done(null, newAccount);
                });
                //return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            } else {

             
            }

        });    
        res.send('success');
    });    

     app.delete('/account/delete/:username', function(req, res) {
         AccountInfo.findOneAndRemove({ 'local.user_id' :  req.params.username }, function(err) {
            // if there are any errors, return the error
            if (err){
                    console.log('found error');
                    res.send(`id: ${req.params.username} not present!`);
            }
                 else {

                    res.send('success');
             }

        });    
      
    });    
}