var express  = require('express');
var app      = express();
var mongoose = require('mongoose');
var passport = require('passport');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var flash    = require('connect-flash');

var configDB = require('./database.js');
var morgan       = require('morgan');
var path=require('path');

mongoose.connect(configDB.url);
app.use(morgan('dev')); 
app.use(bodyParser()); 
app.set('view engine', 'ejs'); 
require('./passport')(passport); 
app.use(session({ secret: 'ilovescotchscotchyscotchscotch'}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash()); 
app.use( express.static(path.join(__dirname,'../../' ,  'dist' ) ) );

//console.log(path.join(__dirname,'../' ,  'client','app', 'reward-page','/reward-page-component.html'));
require('../app/routes/routes.js')(app, passport);
require('../app/routes/account-summary-api.js')(app);
require('../app/routes/settings-api.js')(app);
require('../app/routes/account-info-api.js')(app);
module.exports=app;