var LocalStrategy = require('passport-local').Strategy;


var User = require('../app/model/user');

module.exports = function(passport) {


    passport.serializeUser(function(user, done) {
        
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
              //console.log(user.id+'2');
            done(err, user);
        });
    });

   
    passport.use('local-signup', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true 
    },
    function(req,username, password, done) {

        process.nextTick(function() {


        User.findOne({ 'local.username' :  username }, function(err, user) {
            // if there are any errors, return the error
            if (err){
                    console.log('found error');
                    return done(err);
            }
                

            
            if (user) {
                console.log('no email');
                return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            } else {

                // if there is no user with that email
                // create the user
                var newUser            = new User();

                // set the user's local credentials
                newUser.local.username    = username;
                newUser.local.password = newUser.generateHash(password);//encrypting the password

                console.log('user saved');
                newUser.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, newUser);
                });
            }

        });    

        });

    }));
      passport.use('local-login', new LocalStrategy({
        
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, username, password, done) { // callback with email and password from our form

        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ 'local.username' :  username }, function(err, user) {
            if (err){
                console.log('error');
                return done(err);
            }

            if (!user){
                console.log("invalid username");
                return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash
               
            }
            if (!user.validPassword(password)){
                console.log("invalid password");
                return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata
            }
            console.log('matched ' + user);
            return done(null, user);
        });

    }));
}